import React, { Fragment } from 'react';
import { Table } from 'reactstrap';

const Example = (props) => {
  return (
    <Fragment>
        <tr>
          <th scope="row">1</th>
          <td>TSIBUGAN TIME</td>
          <td>0000-0000-0001</td>
          <td>
            <button className="btn btn-primary ml-1"><i class="far fa-eye"></i></button>
            <button className="btn btn-warning ml-1"><i class="far fa-edit"></i></button>
            <button className="btn btn-danger ml-1"><i class="far fa-trash-alt"></i></button>
          </td>
        </tr>
    </Fragment>
  );
}

export default Example;