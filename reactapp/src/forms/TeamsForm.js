import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const TeamsForm = (props) => {
  return (
    <Form>
      <FormGroup>
        <Label for="" className="mt-3">Team Name</Label>
        <Input type="" name="" id="" placeholder="Please Input Team Name" />
      </FormGroup>

      <div className="text-center"><Button className="mb-3 primary">Create Team</Button></div>
    </Form>
  );
}

export default TeamsForm;