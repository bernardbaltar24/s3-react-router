import React, { useState } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

const RegisterForm = (props) => {

	//STATE
	//object that holds a component's dynamic data
	//useState
	const [username, setUsername] = useState("test")
	console.log(username)

  return (
    <Form>
         <FormGroup>
        <Label for="usernamemail" className="mt-3">Username</Label>
        <Input 
        type="text" 
        name="username" 
        id="username" 
        placeholder="Please input username"
        value={username}
        onChange={e => setUsername(e.target.value)}
         />
        

      </FormGroup>

      <FormGroup>
        <Label for="exampleEmail" className="mt-3">Email</Label>
        <Input type="email" name="email" id="exampleEmail" placeholder="Please input correct Email" />
      </FormGroup>

      <FormGroup>
        <Label for="password">Password</Label>
        <Input type="password" name="password" id="password" placeholder="Please input Password" />
      </FormGroup>

      <FormGroup>
        <Label for="password2">Verify Password</Label>
        <Input type="password2" name="password2" id="password2" placeholder="Please input Password" />
      </FormGroup>

      <div className="text-center"><Button className="mb-3 primary">Submit</Button></div>
      <p>
      Already have an account? <Link to="/login">Login</Link>.
      </p>
    </Form>
  );
}

export default RegisterForm;