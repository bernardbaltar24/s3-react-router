import React from 'react';
import { Table } from 'reactstrap';
import TeamsTableBody from '../rows/TeamsTableBody';

const TaskTables = (props) => {
  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Team Name</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <TeamsTableBody/>
      </tbody>
    </Table>
  );
}

export default TaskTables;